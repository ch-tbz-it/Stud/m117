# Umsetzung (Hands-on) Installation & Konfiguration von zwei Arbeitsplätzen

---

## Ausgangslage: ##

Sämtliche Planungsarbeiten sowie die Installation der IT-Infrastruktur bei der Firma Sota GmbH sind inzwischen abgeschlossen.

Mit diesem finalen Auftrag werden sie nun die Arbeitsstationen **von zwei ihnen zugeteilten Mitarbeitern** gemäss erstelltem IT-Konzept aufsetzen, konfigurieren und deployen.

## Vorgehensweise ##

**1. Namenszuteilung**<br> 
Die Lehrperson teilt Ihnen **zwei** Namen von Mitarbeitern der Firma Sota GmbH zu.

Aufgrund der vorgängig erarbeiteten Systemdokumentation und den abgeschlossenen Teilaufträgen sind sie jetzt in der Lage, diesen letzten Teilauftrag selbständig nach Ihrem Konzept zu erledigen.

**2. Vorbereitung**

Sie benötigen einen Host (am besten ihr Laptop mit optimal 8GB RAM sowie ca. 80GB Festplattenspeicher) mit einer installierten Virtualisierungslösung (Virtualbox, VMware Workstation Player, oder HyperV).

Sie werden **zwei** Virtuelle Maschinen aufsetzen (OS nach Absprache mit Coach).

Diese beiden VMs sollten sich in einem «internen Netzwerk» gegenseitig auf Layer3 (ICMP, Ping) ansprechen können. Bei Windows-Betriebssystemen muss dazu eine eingehende Firewall-Regel für Datei- und Druckerfreigaben erstellt werden. Eine Anleitung finden sie [HIER](https://www.zdv.uni-mainz.de/icmp-ping-in-der-windows-firewall-erlauben/) . 

Damit sie mit beiden VMs auch Zugriff ins Internet haben, empfiehlt sich zudem ein zweiter
Netzwerkadapter – vorzugsweise mit NAT oder Bridge - zu konfigurieren.

Auf dem Bild unten sehen sie ein mögliches Netzwerk-Setup mit Virtualbox oder VMware Workstation Player

![Sota Lab](images/01_sota_lab.jpg)

<br>


>**WICHTIG!** Voraussetzung für das folgende Lab sind **zwei installierte und konfigurierte virtuelle Maschinen**, die sich **gegenseitig auf Layer3 pingen** können. 
>Zumindest eine der beiden VMs benötigt einen zusätzlichen Zugang ins Internet (NAT oder Bridge-Mode). Stellen Sie vorher unbedingt sicher, dass dies funktioniert.


<br><br>

**3. Lab** (Hands-on)

**Voraussetzung**: Beide VMs funktionieren (wie oben vorgeschlagen)

Setzen sie nun diese **beiden** Systeme (VMs) entsprechend den **ihnen zugewiesenen zwei Namen** der Mitarbeiter (siehe oben) auf. Verwenden sie dazu die früher erarbeitete **Konzept- und Systemdokumentation**

**Folgende Tasks** gilt es dabei umzusetzen:


- Erstellen sie zuerst für den **IT-Administratoren** der Firma Sota GmbH einen **Benutzer** mit sämtlichen Administratorenrechen.<br>Als **Passwort** wird der Benutzername verwendet. Führen sie die folgenden Aufgaben **mit diesem Admin-Benutzer** durch:

- Anpassung des **Computernamens**:<br>

- Zuweisung der korrekten **Arbeitsgruppe:** 

- Konfiguration der zugeteilten **IP-Adresse** und **Subnetzmaske**  
(entsprechend den Namen der Mitarbeiter die ihnen zugeteilt wurden)

- Erstellen sie für sämtliche **Mitarbeiter** ein **Benutzerkonto** gemäss
Namenskonvention.   
Als **Passwort** wird der Benutzername verwendet

- Gemäss Organigramm gibt es die Abteilungen Verkauf, Finanzen und Abwicklung.  
**Erstellen** sie sinnvolle **Gruppen** und ordnen sie die Benutzer der entsprechenden
Gruppe zu

- Erstellen sie eine **logische Verzeichnisstruktur** (Ordner) im Verzeichnis
**C:\\Sota.**

- Vergeben sie auf den entsprechenden Verzeichnissen die **Zugriffsrechte** der einzelnen Mitarbeiter.

- Geben sie die erstellten Ordner **einzeln** für den **Netzwerkzugriff** frei


---

...wenn sie mit dem Setup fertig sind:

- **Überprüfen** sie, ob **alles** so funktioniert wie es soll. Loggen sie sich auch mit den unterschiedlichen Benutzern ein und überprüfen sie, ob die Berechtigungen korrekt greifen.
Nutzen sie ihr **Konzept**, das sie vorgängig ausgearbeitet haben, als **Referenz**

- Bereiten sie sich auf die Abnahme vor und melden sie sich dann bei der Lehrperson für die **Live-Demo** an.

---
<br>

## Gutes Gelingen und viel Spass
