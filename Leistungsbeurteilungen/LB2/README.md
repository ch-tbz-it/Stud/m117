## LB2 Projektarbeit 
__(Sozialform vorzugsweise Tandem)__

Bei dieser **Leistungsbeurteilung** handelt es sich um ein SOL-Projekt mit **vier Teilaufträgen**. Um den Lernenden genügend Zeit zur Verfügung zu stellen, sollten ca. **20 Lektionen** dafür eingerechnet werden. Jeder Teilauftrag wird vorzugsweise separat abgegeben (Terminvereinbarung mit der Lehrperson).<br>
**Modulgewichtung: 50%** 

### Gliederung
Die Aufträge der **LB2** sind wie folgt gegliedert:

- [Gesamtauftrag](00_Auftrag_IT-Konzept_1.2.md)
- [Aufbau Physikalische und logische Netzwerktopologie](01_Physik+Log_Netzwerktopologie_v1.2.md) **(15% Gewichtung)**
- [Erstellen einer Offerte (inkl. Material und DL-Kosten)](02_Offerte_erstellen_v1.3.md) **(15% Gewichtung)**
- [Erstellen eines IT-Konzepts](03_IT-Konzept_+_Systemdoku_v1.2.pdf) **(15% Gewichtung)**
- [Installation + Konfiguration von zwei Arbeitsplätzen](04_Installation_+_Konfiguration_v1.2.md) **(55% Gewichtung)**

### Ressourcen
Hier sind Tutorials verlinkt, die bei der Umsetzung der **LB2** nützlich sein können

**Part 1: M117 LB2** - Gathering ISO-Images for your personal Lab-Environment

![Video1:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](images/01_Lab_Setup_part1_200.png)](https://bit.ly/3PHzoDj) **Stream Sharepoint**



---

<br>

**Part 2: M117 LB2** - Setting up a Virtual Machine with **VMware Workstation**

![Video2:](../../x_gitressourcen/Video.png) 8min
[![Tutorial](images/01_Lab_Setup_part2_200.png)](https://bit.ly/4jnsmB5) **Stream Sharepoint**

---

<br>

**Part 3: M117 LB2** - Installing Windows 10 (on a VMware-Workstation VM)

![Video3:](../../x_gitressourcen/Video.png) 7min
[![Tutorial](images/01_Lab_Setup_part3_200.png)](https://bit.ly/3WlvZhb) **Stream Sharepoint**

---

<br>

**Part 4: M117 LB2** - Installing VMware-Tools on Windows 10 (on a VMware-Workstation VM)

![Video4:](../../x_gitressourcen/Video.png) 2min
[![Tutorial](images/01_Lab_Setup_part4_200.png)](https://bit.ly/4g0Prqj)  **Stream Sharepoint**


---

<br>

**Part 5: M117 LB2** - Preparing Network and other Params on Windows 10 (on a VMware-Workstation VM)

![Video5:](../../x_gitressourcen/Video.png) 9min
[![Tutorial](images/01_Lab_Setup_part5_200.png)](https://bit.ly/3CcXbru)   **Stream Sharepoint**


---

<br>

**Part 6: M117 LB2** - Configuring a private Network for 2 VMs (on a VMware-Workstation VM)

![Video6:](../../x_gitressourcen/Video.png) 9min
[![Tutorial](images/01_Lab_Setup_part6_200.png)](https://bit.ly/4jiXrWy)  **Stream Sharepoint**


---

<br>


**Part 7: M117 LB2** - Creating Users, Groups and granting access to specific folders and files (on a VMware-Workstation VM)

![Video7:](../../x_gitressourcen/Video.png) 6min
[![Tutorial](images/01_Lab_Setup_part7_200.png)](https://bit.ly/4gUvjaP) **Stream Sharepoint**


---

<br>

**Part 8: M117 LB2** - Configuring a Directory-share for users and groups on the same Network (on a VMware-Workstation VM)

![Video8:](../../x_gitressourcen/Video.png) 7min
[![Tutorial](images/01_Lab_Setup_part8_200.png)](https://bit.ly/4jlGlY5) **Stream Sharepoint**


<br>


Bei Fragen, Anpassungen oder Wünschen, bitte bei mir melden

Modulverantwortung:
[Marcello Calisto](mailto://marcello.calisto@tbz.ch) 
