# 2. Teilauftrag: Angebot / Offerte erstellen

## Ausgangslage

Sie haben sich mit der Geschäftsleitung abgesprochen und vereinbart, dass sie der Firma Sota GmbH eine **Offerte unterbreiten** werden. 

Im **ersten Teil** dieses Auftrages finden sie die Grundlagen, die für den Aufbau und den Inhalt Ihrer Offerte benötigt wird.

Im **zweiten Teil** erstellen sie eine konkrete Offerte. Das Grundgerüst Ihrer Offerte können sie **unten einsehen**, **übernehmen** und mit **eigenem Inhalt ergänzen**. 

Nachdem die Offerte fertig ausgearbeitet ist, werden sie diese der Geschäftsleitung der Firma Sota GmbH unterbreiten. Dazu vereinbaren sie mit dem CEO der Firma (in diesem Fall der Lehrperson) ein Meeting, wo sie Ihr **Konzept mit den kalkulierten Kosten** präsentieren.

Bereiten sie sich entsprechend darauf vor und notieren Sie sich **stichfeste** Argumente. 

**Je überzeugender Sie diese "verkaufen", desto grösser ist die Chance, dass sie den Zuschlag erhalten.**

Ziel dieses Auftrages ist es, dem Kunden Ihr Projekt **so schmackhaft wie möglich** zu machen und die Ausschreibung zu gewinnen. Achten sie darauf, dass die Kosten so kalkuliert sind, dass sie am Ende auch einen Gewinn ausweisen können. Qualität hat seinen Preis. Achten sie trotzdem darauf, dass sie nicht übermässig teurer sind als Ihre Konkurrenz. Je besser das Preis/Leistungsverhältnis, desto höher ihre Chancen.

<br>

## Inhalt der Offerte
Die Offerte sollte folgende **Schwerpunkte** abdecken:

- **Materialkosten** (Preisliste mit sämtlichen Positionen)

- **Dienstleistungskosten** (Aufwand Installation und Konfiguration der gesamten IT-Infrastruktur)

- **Wartung** (Support) - Optional, ergänzende Dienstleistung

- **Angebotsbedingungen** (Angebot, Rechnungsstellung, Zahlungsbedingungen, Haftung, Gerichtsstand, Gültigkeit des Angebots)

<br>

## Sinnvolle Vorgehensweise

### 1. Materialliste erstellen und berechnen
(Liste mit sämtlichen Posten plus Totalbetrag) 

Beim Zusammenstellen der Offerte ist es hilfreich, wenn sie sich zuerst mit dem zu beschaffenden Material auseinandersetzen. Folgende Fragen unterstützen Sie dabei:
1. Welche IT-Geräte und welches Zusatzmaterial ist notwendig und erfüllt die Kundenbedürfnisse am besten? Versetzen sie sich in die Lage des Kunden.
2. Erstellen sie eine Materialliste. Listen Sie sämtliche Komponenten auf. Es sollte dabei möglichst **NICHTS** vergessen gehen (sonst überziehen sie später das Budget oder vermindern den Gewinn ihrer Firma).

Recherchieren sie im Internet. Referenzieren sie Lieferant, Hersteller, Typ, Menge und Preis (inkl. MwST.). Diese sollen eindeutig und nachvollziehbar auf der Materialliste aufgeführt sein.


### 2. Dienstleistungen zusammenstellen und berechnen

Den grössten Gewinnanteil erwirtschaftet Ihre Firma mit den zu erbringenden Dienstleistungen. Hier können Sie die Arbeit Ihrer Mitarbeiter und die Arbeit Ihrer Geschäftspartner verrechnen.

Die Dienstleistungen setzen sich aus **14** Positionen zusammen und sind für Sie und alle anderen Offertanbieter vorgegeben (unten detailliert aufgelistet und teilweise bereits ausgefüllt). 
**Ein Arbeitstag** wird mit **8std.** verrechnet. 

 Bei der Kalkulation der Dienstleistungen orientieren wir uns an den Honorar-Empfehlungen vom [KBOB](https://www.kbob.admin.ch/kbob/de/home/themen-leistungen.html) . Die Stundensätzen im ICT-Sektor sind frei erfunden.


1. Erstellen sie eine eigene Kostenmatrix für die Dienstleistungen und füllen Sie sämtliche Felder korrekt aus. Achten sie darauf, dass für die jeweiligen Arbeiten die richtigen Berufsbilder mit den entsprechenden Stundensätzen verwendet werden. Ein Arbeitstag wird mit 8std. verrechnet.

2. Berechnen sie sämtliche Dienstleistungen (Kostenmatrix aufführen. Nebenkosten müssen nicht berücksichtigt werden


**Honorarkategorien**

| Kategorie   |   A   |   B   |   C   |   D   |   E   |   F   |
|-------------|-------|-------|-------|-------|-------|-------|
| Stundensatz | 230.- | 180.- | 130.- | 110.- | 100.- |  80.- |

**Berufsbilder kategorisiert**

|  Kategorie  | A | B | C | D | E | F |
|-------------|---|---|---|---|---|---|
| Bezeichnung | Consulting Engineering | Projektleiter, Bau-Architekt, Ausbildner | Systemtechniker | Logistiker Betriebsinformatiker | Elektriker | Hilfskraft |

**Kalkulationstabelle**

| Pos. | Kurzbeschrieb                                                                                                         | Tarif | Stunden | Betrag        |
|------|-----------------------------------------------------------------------------------------------------------------------|-------|---------|---------------|
| 1 | Consulting (Besprechungen, Beratung, Aufnahme vor Ort, Abklärungen) | | 12 | CHF 2‘760.-- |
| 2    | Elektriker UGV (Auftrag an externe Firma) 2 Mitarbeiter à je 20 Std. (Elektriker)  |  |  |  CHF  |
| 3 | Logistik (Bestellung, Lieferung, Aufbau der IT-Komponenten) Logistiker, 1 Tag  | D     |  |  CHF |
| 4    | Betriebshandbuch und Systemdokumentation erstellen Betriebsinformatiker 2 Tage  |  |  |  CHF  |
| 5 | Offline Installation ICT-Komponenten (Netzwerkgeräte, Computer OS, Drucker) Elektriker, 3 Tage |   | |  CHF  |
| 6 | Konfiguration der ICT-Komponenten (Netzwerk, Benutzer, Gruppen, Berechtigungen) Systemtechniker, 2 Tage | C  |  |  CHF |
| 7 | Nearline Installation der ICT-Komponenten (Netzwerkgeräte, Computer, Drucker) Elektriker, 2 Tage)                |       |   |  CHF  |
| 8 | Online Installation der ICT-Komponenten (Umstellung und einbinden in die produktive Umgebung) Systemtechniker, 2 Tage |  | 16  | CHF |
| 9  | Anpassungen und Troubleshooting Systemtechniker 1 ½ Tage   |  |  |  CHF |
| 10  | Benutzer schulen (Ausbildner) 10 Std. Abo gebucht, wird auf Anfrage bezogen  |  | | CHF |
| 11  | Transition in die produktive Umgebung (Übergabe mit Protokoll) Engineering, ½ Tag                                     |  |  |  CHF  |
| 12  | Tests Betriebsinformatiker  |  | 2 |  CHF  |
| 13  | Support während der Startphase (Pauschalbetrag) | /     | /  |  CHF 2‘000.-- |
| 14 | Diverses    |  |  |  |
|  | Total Dienstleistungen exkl. MWSt. | | |  CHF  |
|  | MWst. | 8%  |  |  CHF  |
|  | Total Dienstleistungen |  |  |  CHF  |

<br>

### 3. Offerte erstellen

Sämtliche **Material- und Diensleistungskosten** sind nun berechnet. 

Sie haben jetzt also alle Fakten, um eine Offerte zu erstellen. Es steht ihnen frei, im Internet nach möglichen Beispielen oder Vorlagen zu suchen. Die Inhalte weiter unten sind ebenfalls Bestandteil dieser Offerte und sollen fachgerecht übernommen werden. Beachten Sie, dass dieses Angebot verbindlich ist (mögliche Fehler könnten Sie also später teuer zu stehen kommen).

<br>

## Ein mögliches Beispiel einer Offerte

Bitte beachten Sie, dass dies lediglich ein unformatierter "Draft" ist. Eine Offerte sollte selbstverständlich sämtliche formellen Vorgaben erfüllen, fehlerfrei geschrieben und ansprechend formatiert sein.
<br>

### Unser Angebot

Sehr geehrte/r Frau/Herr ...<br>
Es freut uns sehr ....

Anbei unsere Offerte mit den einzeln aufgeführten Positionen unserer Dienstleistung:

Der Aufbau unseres Angebotes orientiert sich an der Vereinbarung vom xx.xx.xxxx

Unser Angebot enthält die Leistungen für den gesamten Projektablauf,
insbesondere:

-   Arbeiten der Elektriker
-   Verkabelung des Gebäudes
-   Neuanschaffung und Auftragsvergabe an Subunternehmen
-   Installation neuer ICT-Komponenten
-   Konfiguration und Inbetriebnahme der neuen ICT-Komponenten
-   Betriebs- und Systemdokumentation
-   Qualitätskontrolle und begleitete Service-Transition in den Betrieb
-   Ausbildung / Schulung der Mitarbeiter
-   Support während der Startphase (Pauschal)


**Materialkosten** (inkl. Mehrwertsteuer) **CHF** __________

**Dienstleistungskosten** (inkl. Mehrwertsteuer) **CHF** ____________

**Angebotspreis Total** (inkl. Mehrwertsteuer) **CHF** __________

**Weitere Angebotsbedingungen**

Zu Auftragsbeginn sind die vollständigen im Angebot genannten Unterlagen in Papierform und auf Datenträger vom Auftraggeber zur Verfügung zu stellen. Der Auftraggeber haftet für die Richtigkeit der Unterlagen. Projektbeeinflussende Änderungen in diesen Unterlagen wird der Auftraggeber uns umgehend mitteilen.

**Rechnungsstellung / Zahlungsbedingungen**

Die Rechungsstellung erfolgt monatlich mit einem Zahlungsziel von 30 Tagen
netto. Eine Skontierung ist ausgeschlossen.

**Haftung**

Unsere Haftung, egal aus welchem Rechtsgrund, ist ausgeschlossen, soweit die Pflichtverletzung, die zur Schädigung geführt hat auf fahrlässigem Handeln beruht. Die vorstehende Haftungsbegrenzung gilt nicht für die Verletzung von wesentlichen Vertragspflichten. Die Haftung begrenzt sich in diesem Falle jedoch auf nach dem Vertragszweck und bei Vertragsschluss vorhersehbare, typische
Schäden.

Haftungsausschluss und Haftungsbegrenzung gelten insgesamt nicht bei Schäden aus der Verletzung des Lebens, des Körpers oder der Gesundheit. Werden durch vorsätzliche Handlungen oder durch grobes Verschulden Schäden verursacht, für die der AN einzustehen hat, so haftet er nach den gesetzlichen Vorschriften

**Gerichtsstand:**

Für dieses Angebot und die Auftragsabwicklung gilt Schweizer Recht. Der Gerichtsstand ist Zürich.

**Gültigkeit des Angebots:**

Wir halten uns 3 Monate an unser Angebot gebunden.

**AGB:**

Darüber hinaus gelten die die Allgemeinen Geschäftsbedingungen der Firma "Ihre Firma", welche unter www.ihrefirma.ch eingesehen und heruntergeladen werden können.

Mit freundlichen Grüssen
<br><br>Unterschrift<br><br><br>
"Ihre Firma"

Geschäftsführer (Lernender) Kunde (Lehrperson)

<br>


    Unklarheiten können jederzeit direkt mit dem Kunden (Lehrperson) geklärt werden.
