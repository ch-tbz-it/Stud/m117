# 1. Teilauftrag: 
## Physikalische & Logische Netzwerktopologie:

Die Geschäftsleitung der Firma Sota GmbH hat entschieden, dass das Personal erst zügelt, wenn die neue IT-Infrastruktur bereitsteht. Ihre Firma hat soeben den Gebäudeplan erhalten. Es ist also Zeit, mit der Planung des IT-Infrastruktur Konzepts zu beginnen.

Es bestehen folgende Voraussetzungen:

- Alle Räumlichkeiten der Firma befinden sich im Erdgeschoss.

- In allen Wänden der Räume sind bereits moderne Kabelrohre eingelassen; so dass alle
Kabel sauber in diese Kanäle geführt werden können

- Die Kabeldurchführung in benachbarte Räume ist ebenfalls gewährleistet

- Die Steckdosen in der Nähe der IT-Geräte müssen sie selber anbringen und verkabeln

- Bei einem allfälligen Mieterwechsel soll der Nachmieter die Grundinstallation weiter benutzen können

- Die Firma will keine fix installierte WLAN-Umgebung. Ein entsprechender WLAN-Router steht zwar zur Verfügung, wird aber nur in Ausnahmefällen an einen vorhandenen, freien Switch-Port angeschlossen. Diese Umgebung wird bewusst nicht in der Gebäudeverkabelung aufgeführt (muss aber beschafft werden und somit auf der Materialliste vorhanden sein).


        Hinweis: Die nachfolgende Zeichnung ist nicht Massstabgetreu.
        Sie benötigen lediglich die Distanzen und die Anordnung der Geräte in den Räumen

<br><br>

## Vorgehensweise

1.  Entscheiden sie sich, wie sie das Gebäude am optimalsten verkabeln. Gehen sie dabei systematisch und überlegt vor. Welche Verbindungen können sie mit fixen und welche mit fliegenden Kabeln sicherstellen. Zeichnen Sie diese ein und erstellen Sie dazu eine Legende (z.B. Rot UGV / Blau Fliegend etc...)


        Fliegende Verkabelung (Blaue Farbe verwenden)
        UGV, Fixe Verkabelung (Rote Farbe verwenden)


### Gebäudeplan
![Gebäudeplan](images/00_Buero_Plan.jpg)

Verwenden Sie eines der drei folgenden Originalfiles (Visio / draw.io) für Ihre Einträge:

- [Visio-File im **.vsd** Format](images/00_Buero_Plan.vsd) 
- [Visio-File im **.vsdx** Format](images/00_Buero_Plan.vsdx)
- [Draw.io-File **.drawio** Format](images/00_Buero_Plan.drawio) _(Alternative zu Visio)_


<br>

2.  Entwerfen Sie den **logischen Netzwerkplan**<br>
Dieser sollte genau der Gebäudeverkabelung entsprechenden (IT-Geräte sollen anhand der neuen Gebäudeverkabelung korrekt abgebildet und bezeichnet sein). <br><br>

**Tipp:**
Als Vorlage können Sie den Netzwerkplan aus der Anfangsübung (Heimnetzwerk) nutzen
