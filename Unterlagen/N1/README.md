# N1 - Niveau 1: "Basic Skills"

In diesem Verzeichnis sind die Arbeitsaufträge und Arbeiten des **Kompetenzniveaus "1"** (aufbauend) abgelegt. Die Unterlagen werden laufend ergänzt oder angepasst und können bei Bedarf genutzt werden.

## Arbeitsaufträge / Unterlagen

- [Heimnetzwerk kennenlernen und dokumentieren - Praxisteil](m117_1_Heimnetzwerk_dokumentieren.md) -
 [(_Link zu Beispiele von Lernenden_)](ressourcen/1-beispiele)
- [Verkabelungsarten, Ethernettechnologien](m117_2-Kabel_Verkabelungsarten_Ethernettechnologien.md)
- [Netzwerkeinstellungen und Adressen - Theorie](m117_3_Netzwerkeinstellungen_und_Adressen_Theorie.md)
- [Netzwerkeinstellungen und Adressen - Windows Hands-on (Vorbereitung LB2)](m117_4_Netzwerkeinstellungen_und_Adressen_Praxis.md)

## Hands on

#### 1. Auftrag 
- [Filius-Auftrag](m117_5_Filius-Hands-on.md) (erste Anwendung mit der Lernsoftware **"Filius"**)


#### 2. Auftrag 
- [Filius-Auftrag](ressourcen/2-auftraege-LN/02) (Netzwerk für Kleinbetrieb - mit der Lernsoftware **"Filius"**)

#### 3. Auftrag 
- [Filius-Auftrag](ressourcen/2-auftraege-LN/03) (**Folgeauftrag**: Netzwerk für Kleinbetrieb - mit der Lernsoftware **"Filius"**)

<br>

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---