# Praxisteil

<br>

## Netzwerk installieren

Die Netzwerkkonfiguration umfasst drei Hauptteile:

-   **Netzwerkkarte** (dazu gehören auch die richtigen Kabel und Stecker)

-   **Netzwerkprotokoll**

-   **Netzwerkdienste**

<br>

## Netzwerkkarte installieren und kontrollieren

Als erstes muss die korrekte Installation der Netzwerkkartentreiber kontrolliert werden. Die Installation des Treibers für die Netzwerkkarten wird bei modernen Betriebssystemen automatisch ablaufen. Ansonsten müssen die Treiber besorgt (vom Hersteller) und installiert werden.

**![Geraete-Manager](images/40c-Mittel_800.png)**

<br>

| **Teilziel** | **Vorgehen** | **Test/Bemerkungen**  | **Fehler** |
|---|---|---|---|
| Kontrolle der Netzwerkkarten-Installation | In Geräte-Manager mit Gegenstelle verkabeln. Link-Light überprüfen | Die Netzwerkkarte darf keinen Fehler aufweisen. Bei Fehlern wird eines der Symbole rechts angezeigt. Link-Light muss brennen | ![Fragezeichen](images/41.png) ![Ausrufezeichen](images/42.png)


---

Treiber nicht erkannt |  Treiber fehlerhaft
:-------------------------:|:-------------------------:
![Fragezeichen](images/41.png)    |  ![Ausrufezeichen](images/42.png)

---

<br>

## Netzwerkprotokolle kontrollieren oder installieren

Eine Netzwerkverbindung benötigt nicht nur eine Netzwerkkarte, sondern auch ein **Protokoll**, das ist die Kommunikationssprache, die über diese Netzwerkkarte läuft.  
Ein Protokoll ist mit einer Sprache zu vergleichen. Während wir uns in der
deutschen Sprache unterhalten, unterhält sich der Computer über eine digitale
Sprache, z.B. TCP oder IP.   
Um zu kontrollieren, ob das richtige Protokoll installiert ist, müssen die
**Eigenschaften** der **Netzwerkverbindung** aufgerufen werden. Fehlende
Protokolle können über „Installieren“ hinzugefügt werden.  
**Hinweis:** Es können mehrere (unterschiedliche) Protokolle pro Netzwerkkarte installiert sein.

![](images/43-Gross.png)

| **Teilziel** | **Vorgehen** | **Test / Bemerkungen**  |
|---|---|---|
| Kontrolle oder Installation des Netzwerkprotokolls | Netzwerk- und Freigabecenter -\> Verbindungen -\> Eigenschaften  (Alle vorhandenen Verbindungen sind unter „Adaptereinstellungen ändern „sichtbar) | Das entsprechende Protokoll **„Internetprotokoll Version 4 (TCP/IP v4)“** ist vorhanden und aktiviert (Häkchen). 

<br>


## Netzwerkeinstellungen konfigurieren

Ein Netzwerkprotokoll muss korrekt **konfiguriert** sein, um mit den anderen Computern im Netzwerk zu funktionieren. Ein oft verwendeter Vorgang, ist die
Benutzung von **automatisch zugewiesenen IP-Adressen mit dem Netzwerkdienst DHCP**. Diesen Dienst werden sie im **Modul 123** noch besser kennenlernen.

<br>

## Gateway (Router)

Das Gateway (auch Standardgateway genannt) ist der „Ausgang“ aus einem Netzwerk zum nächsten Netzwerk (z.B. das Internet). Im Netzwerk übernimmt der **Router** die Funktion des Gateways. **Der Router erhält**, wie jedes andere Gerät im Netzwerk, **auch eine IP-Adresse** aus diesem Netzwerk. Die Gateway-Einstellung ist für alle Geräte, die sich im gleichen Netzwerk befinden, gleich
(es hat nur einen Gateway). Diesem Gateway weist man üblicherweise die **tiefste oder die höchste mögliche Adresse im Netzwerk** zu:

Diese Angabe ist nur dann notwendig, wenn ein anderes Netzwerk zum bestehenden angeschlossen wird. 
In diesem Modul wird nicht weiter darauf eingegangen (Vertiefung erfolgt in den **Modulen 123 und 129**). 

Bei einem Internetanschluss, wie später bei der LB2 Projektarbeit (Reorganisation der Firma Sota GmbH), ist dies vom Provider vorgegeben.  

**Hinweis:** Bei der Konfiguration eines Gerätes ist es immer sinnvoll, den Gateway möglichst früh zu bestimmen, zu konfigurieren oder zu testen. Oftmals ist dies der Grund, weshalb die Verbindung zur Aussenwelt nicht funktioniert.

### Anwendungen in der Praxis:

**Symbol:** Das Symbol des Routers ist sehr ähnlich wie das Symbol des Switches. Im Gegensatz zum Switch zeigen die beiden horizontalen Pfeile nach aussen, während die vertikalen Pfeile nach innen gerichtet sind. Beim Switch zeigen alle vier Pfeile horizontal nach aussen.

**Small Business Router oder Enterprise:** Im Vergleich zu Routern in der Home-Nutzung, werden im KMU- oder Enterprise-Sektor leistungsstärkere, teilweise skalierbare, Router mit zusätzlichen Optionen verwendet.

Symbol | Small Business | Enterprise
:---:|:---:|:---: 
![Privat](images/44a-Router-Symbol.png) | ![Privat](images/44b-Router_SmallBusin.jpg) | ![Enterprise](images/44c-Router-Enterp_klein.png)    |

<br>

## DNS-Server

Eine weitere Einstellung, ist die Angabe eines **DNS-Servers**.  
Der DNS-Dienst (Domain Name System) kann als eine Art **Auskunftsstelle** im Internet angesehen werden. Er liefert **zu einem Namen** im Internet (z.B. www.tbz.ch) **die zugehörige IP-Adresse**.   
Wenn jemand im Webbrowser die Seite www.tbz.ch öffnen will, muss der Browser die IP-Adresse von www.tbz.ch wissen. Dazu fragt er den **DNS-Server** nach der IP-Adresse von www.tbz.ch und erhält vom DNS-Server die zugehörige IP-Adresse.

Damit dies funktioniert, muss dem Computer die **IP-Adresse eines DNS-Servers** konfiguriert werden. Der DNS-Server kann lokal im eigenen Netz sein oder irgendwo im Internet (Oft ist es der DNS-Server des Internet-Providers). Der DNS-Dienst wird später im **Modul 123** ausführlich behandelt.

**Hinweis:** Normalerweise werden zwei DNS-Server angegeben. Sollte der eine nicht erreichbar sein, wird die Anfrage an den zweiten gestellt.

**Mögliche Konfiguration eines Hosts** (in diesem Fall Windows 10). Um in das Internet zu gelangen, muss die Netzwerkverbindung zuerst entsprechend konfiguriert werden. Das folgende Beispiel zeigt auf, wie man einem Gerät eine statische IP-Adresse zuweist. Dabei gilt zu beachten, dass kein DHCP-Server mehr zur Verfügung steht und die Werte mit dem entsprechenden Netzwerk übereinstimmen **müssen**. Wenn ich also später mit diesem Gerät in ein anderes WLAN wechsle (z.B von zu Hause in die Schule), kann das Gerät **keine Verbindung zum Internet** herstellen. Aus diesem Grund lohnt es sich im Normalfall, **DHCP** als Standard eingestellt zu lassen (IP-Adresse automatisch beziehen)

| **Fixe IP** | **Subnetzmaske** | **Standardgateway**  | **Bevorzugter DNS** (in diesem Fall Google-DNS) | **Alternativer DNS** (in diesem Fall Google-DNS)
|---|---|---|---|---|
| 192.168.0.20 | 255.255.255.0 | 192.168.0.1 | 8.8.8.8 | 8.8.4.4

![](images/45a-Gateway_DNS_1200.jpg)

| **Teilziel** | **Vorgehen**  | **Test/Bemerkungen** | **Kontrollvisum** |
|---|---|---|---|
| Konfiguration des Netzwerkprotokolls | **Eigenschaften** des entsprechenden Netzwerkprotokolls | Jede Arbeitsstation braucht eine eindeutige IP-Adresse (Host-ID), muss aber im gleichen Adressbereich (Netz-ID) sein.  | Selbstkontrolle   |

<br>

## Computername
Mit **Computername** kennzeichnen sie die eigene Arbeitsstation. Unter diesem
Namen erscheint die Station unter „Netzwerk“ im Windows-Netz (NETBIOS-Computername) und derselbe Name wird normalerweise auch als DNS-Namen
der Maschine genutzt.   
Wenn Sie den Computernamen oder die Arbeitsgruppe ändern, müssen Sie unter
Windows neu booten.

Bei der **LB2** werden Sie den Namen Ihrer PCs (VMs) gemäss einem **von Ihnen ausgearbeiteten Namenskonzeptes** anpassen.

**Vorgehensweise beim Ändern eines PC-Namens:**
- Auf das Windows-Symbol unten links klicken
- Den Begriff "pc" eingeben und danach "PC-Infos" auswählen. Es erscheint ein Menü mit sämtlichen Angaben zu Ihrem PC (Gerätename, Prozessor, Installierter RAM, Geräte-ID etc...)
- Auf den Button "Diesen PC umbenennen" klicken und den **neuen Namen** eingeben.
- Bestätigen und rebooten

PC Informationen suchen | PC-Name umbenennen
:---:|:---:
![](images/46b-PC-Namen_400.jpg)    |  ![](images/46a-PC-Namen_800.png)

<br>

## Arbeitsgruppe

Mit dem **Arbeitsgruppen-Namen** wird definiert, welche Computer zur selben Windows-Arbeitsgruppe gehören. **Alle** Computer mit **demselben** Arbeitsgruppen-Namen **sehen sich** dann unter „Netzwerk“.

Bei der **LB2** werden Sie die Arbeitsgruppe Ihrer PCs (VMs) gemäss einem **Namenskonzept** konfigurieren.

Arbeitsgruppenfeld suchen | Arbeitsgruppe umbenennen
:---:|:---:
![](images/47a-Arbeitsgruppe_400.png)    |  ![](images/47b-Arbeitsgruppe_400.png)

<br>


## Primäres DNS-Suffix des Computers

Mit „**Primäres DNS-Suffix des Computers**“ wird die Zuordnung der Arbeitsstation (Host) zu einer eigenen DNS-Domäne definiert, d.h. sie können
intern ihren eigenen Namen wie die Internetnamen definieren.   
Dabei sollten sie einfache Namen ohne Punkte verwenden, v.a. aber **keine** die aussehen wie (echte) Internetnamen (z.B. switch.ch, microsoft.com), sonst können sie nicht mehr übers Internet auf die betreffenden Domänen zugreifen.

Diese Einstellung **muss nicht zwingend** die gleiche Einstellung, wie die im lokalen Netzwerk sein. Im Internet herrschen andere Regeln für die Namenszuordnung als im LAN.

Verwenden sie bei der **LB2** den DNS-Suffix: **sotagmbh** (wie im Bild unten ersichtlich)


![](images/48-suffix_800.png)


| **Teilziel** | **Vorgehen**  | **Test/Bemerkungen** | **Kontrollvisum** |
|---|---|---|---|
| Konfiguration des Computer-Namens, der Arbeitsgruppe und des DNS-Suffix | Systemsteuerung -\> System | Kontrolle (nach Neustart) | Selbstkontrolle   |

<br>


## Netzwerkdienste kontrollieren oder installieren

Als Benutzer wollen Sie schlussendlich Dienste des Netzwerkes nutzen. Die beiden **am häufigsten gebrauchten Dienste** die Windows von Haus aus bietet sind:

- Zugriff auf Verzeichnisse von anderen Computer
- Drucken auf Drucker, die an anderen Computern angeschlossen sind.

Damit Sie diese Dienste nutzen können, müssen:

-   der **Client für Microsoft Netzwerke**   
    (damit können Sie auf andere Computerzugreifen)
-   die **Datei- und Druckerfreigabe** für Microsoft-Netzwerke (damit können andere auf Ihrem Computer zugreifen)

installiert und aktiviert (d.h. mit Häkchen versehen) sein.

![Dienste](images/49-Dienste_800.png)

| **Teilziel** | **Vorgehen**  | **Test/Bemerkungen** | **Kontrollvisum** |
|---|---|---|---|
| Aktive Dienste (MS-Netzwerk), Datei- & Druckerfreigabe | Netzwerk- und Freigabecenter -\>  Verbindungen -\>  Eigenschaften  | Die gewünschten Clients und Dienste erscheinen in Netzwerk-Eigenschaften | Selbstkontrolle   |

<br>



## Kontrolle der Netzwerk-Einstellungen

Starten sie eine **Eingabeaufforderung** und kontrollieren sie ihre eingegebenen
Angaben mit der Ausgabe   
von **ipconfig /all***.*

> `ipconfig /all ` _im Terminal (cmd) absetzen_<br>

![ipconfig](images/50b-ipconfig_800.png)

| **Teilziel** | **Vorgehen**  | **Test/Bemerkungen** | **Kontrollvisum** |
|---|---|---|---|
| Kontrolle der Netzwerk-Einstellungen | Start -\> **cmd** eingeben Befehl : **ipconfig / all**  | Achten sie auf den richtigen  Ethernet-Adapter | Selbstkontrolle   |

<br>

## Kontrolle der Verbindungen

### Verbindung mit Nachbarn

Kontrollieren Sie, ob Sie **die anderen Arbeitsstationen** im Netzwerk mit
**Ping** erreichen können.

**Hinweis:** Ping ist ein Diagnose-Werkzeug, mit dem überprüft werden kann, ob ein bestimmter Host (Computer) in einem IP-Netzwerk erreichbar ist. Dazu sendet **"Ping"** eine Anfrage an den gewünschten Host und dieser gibt eine Antwort (falls er erreichbar ist).

> `ping 192.168.100.1 ` _Ping auf ein anderes Gerät im Netzwerk_<br>

![ipconfig](images/51-ping_800.png)

| **Teilziel** | **Vorgehen**  | **Test/Bemerkungen** | **Kontrollvisum** |
|---|---|---|---|
| Kontrolle des Nachbarn | Start -\> **cmd** eingeben. Befehl : **ping** \<Nachbar\> | Nachbaradressen sind in Ihrem definierten Bereich. Fragen sie ihren Nachbarn, ob er "lebt" und nach seiner MAC-Adresse (für den ARP-Cache) | Selbstkontrolle   |


Wenn in Ihrem Netzwerk auch das Standardgateway vorhanden ist, können Sie versuchen, auch dieses zu erreichen.  

**Hinweis:** Nicht jedes Gerät gibt (aus Sicherheitsüberlegungen) eine Antwort
auf eine Ping-Anfrage.   
Dazu gehört z.B. auch ein Windows-PC, siehe dazu weiter unten.

### Windows und ping

**Achtung:** Ein Host ist nicht verpflichtet eine Antwort zu schicken und einige machen das auch nicht – Windows (ab Windows 7) gehört auch zu denen…  
Die **Firewall** in Windows betrachtet einen Ping als bösen Angriff und **antwortet daher nicht** auf den einkommenden Ping Anfrage. Soll Windows eine
Antwort geben, **muss die entsprechende Firewall-Regel angepasst werden.**

**Vorgehen:**   
- **Start** -\> „Firewall” eintippen -\> Erweiterte Einstellungen -\> Eingehende Regeln  
- **Regel**: „Datei- und Druckerfreigabe (Echoanforderung – ICMPv4 eingehend)“
**aktivieren**

**Tipp:** Falls das Bild nicht lesbar ist, drauf klicken

[![fw-regel](images/52b-FW-Freigabe_800.png)](images/13-FW-Freigabe.png)

So gibt Windows freundlich Antwort auf eingehende Pings…

<br>

### Kontrolle der Verbindung mit der eigenen Adresse

Wenn die Verbindung zu den Nachbarn nicht klappt, muss zuerst überprüft werden,
ob sich die **eigene IP-Adresse** anpingen lässt.  
Dazu wird ping auf die **eigene Adresse** ausgeführt oder auf die reservierte Adresse **127.0.0.1**.   
**127.0.0.1** bedeutete immer **"meine IP-Adresse"** oder **"Localhost"** (im folgenden Bild 192.168.0.150). Die beiden Tests sind somit identisch.   
Falls dieser Test nicht funktioniert, sind die Protokolle nicht korrekt installiert oder konfiguriert. Gehen sie zurück zu den entsprechenden Schritten und überprüfen Sie den Ablauf nochmals.

[![localhost](images/53b-ping-localhost_800.png)](images/53a-ping-localhost.png)


<br>

## Kontrolle der Netzwerkdienste

Ein einfacher Test ob die Datei- und Druckerfreigabe innerhalb einer Arbeitsgruppe funktioniert ist, nachzuschauen, ob Sie die anderen Computer ihrer
Arbeitsgruppe in der Netzwerkumgebung sehen können.   

Sie können auch ein Verzeichnis im Netzwerk freigeben (rechte Maustaste auf
Verzeichnis -\> Freigabe) und kontrollieren, ob Sie es in Ihrer Netzwerkumgebung und auf allen Computern der Arbeitsgruppe sehen.

| **Teilziel** | **Vorgehen**|**Test/Bemerkungen**  | **Kontrolle**|
|---|---|---|---|
| Kontrolle der Netzwerkdienste | Start -\> Computer -\> Netzwerk  Testordner im Netzwerk freigeben | Sehe ich die anderen Computer unter Netzwerkumgebung? Sehe ich den freigegebenen Ordner? | Selbstkontrolle |


<br>

## Plannungsformular / Checkliste

| **Teilziel**|**Vorgehen**| **Test/Bemerkungen** | **Kontrollvisum**|
|---|---|---|---|
| Kontrolle der Netzwerkkarten-Installation | In Geräte-Manager. Mit Gegenstelle verkabeln. Link-Light überprüfen  | Die Netzwerkkarte darf keinen Fehler aufweisen. Link-Light muss brennen. Bei Fehlern wird eines dieser Symbole angezeigt. ![nr3](images/41.png) ![nr2](images/42.png) | Selbstkontrolle |
| Kontrolle oder Installation des Netzwerkprotokolls | Netzwerk- und Freigabecenter -\> Verbindungen -\> Eigenschaften  (Alle vorhandenen Verbindungen sind unter „Adaptereinstellungen ändern „sichtbar) | Das entsprechende Protokoll **„Internetprotokoll Version 4 (TCP/IP v4)“** ist vorhanden und aktiviert (Häkchen) | Selbstkontrolle |
| Konfiguration des Netzwerkprotokolls| **Eigenschaften** des entsprechenden Netzwerkprotokolls.| Jede Arbeitsstation braucht eine eindeutige IP-Adresse (Host-ID), muss aber im gleichen Adressbereich (Netz-ID) sein.   | Selbstkontrolle  |
| Konfiguration des Computer-Namens, der Arbeitsgruppe und des DNS-Suffix | Systemsteuerung -\> System | Kontrolle (nach Neustart)   | Selbstkontrolle   |
| Kontrolle oder Installation von Client und Dienst | Netzwerk- und Freigabecenter -\> Verbindungen -\> Eigenschaften  (Alle vorhandenen Verbindungen sind unter „Adaptereinstellungen ändern „sichtbar) | Die gewünschten Clients und Dienste erscheinen in Netzwerk-Eigenschaften und sind aktiviert (Häkchen), sonst muss es installiert werden.| Selbstkontrolle  |
| Kontrolle der Netzwerk-Einstellungen | Start **cmd** eingeben Befehl : **ipconfig /all**  | Achten sie auf den richtigen  Ethernet-Adapter | Selbstkontrolle   |
| Kontrolle des Nachbarn  | Start **cmd** eingeben Befehl: **ping** **\<Nachbar\>**  | Nachbaradressen sind in Ihrem definierten Bereich. Fragen sie ihren Nachbarn nach seiner Adresse..   | Selbstkontrolle   |
| Kontrolle der Netzwerkdienste | Start -\> Computer -\> Netzwerk  Testordner im Netzwerk freigeben     | Sehe ich die anderen Computer unter Netzwerkumgebung? Sehe ich den freigegebenen Ordner?  | Selbstkontrolle   |