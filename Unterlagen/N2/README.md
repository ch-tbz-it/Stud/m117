# N2 - Niveau 2: "Advanced Skills"

In diesem Verzeichnis sind die Arbeitsaufträge und Arbeiten des **Kompetenzniveaus "2"** (aufbauend) abgelegt. Die Unterlagen werden laufend ergänzt oder angepasst und können bei Bedarf genutzt werden.

## Arbeitsaufträge / Unterlagen

- [Subnetting - Theorie _(Vertiefung)_](m117_4_Subnetting.md)
- [Subnetting - Übung _(mit Excelsheet)_](m117_4_subnetting_uebung.md)
- [Benutzer, Gruppen, Berechtigungen und Freigaben - Theorie](m117_5_Gruppen_und_Berechtigungen.md)



## Hands on

**1. Auftrag** 
- [Subnetting im letzten Oktett](ressourcen/2-auftraege-LN/P0/README.md) _(Veränderung der Subnetzmaske und dessen Auswirkungen)_

**2. Auftrag** 
- [Filius-Auftrag](ressourcen/2-auftraege-LN/P1/README.md) _(Netzwerk für ein mittelgrosses Unternehmen)_

**3. Auftrag** 
- [Filius-Auftrag](ressourcen/2-auftraege-LN/P2/README.md) _(Subnetting: Herr des Rings)_


<br>

---

> [⇧ **Zurück zu Unterlagen**](../README.md) 

---

> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---