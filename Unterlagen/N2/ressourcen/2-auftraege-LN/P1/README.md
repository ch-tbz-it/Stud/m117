# Netzwerk für ein mittelgrosses Unternehmen

### Arbeitsauftrag:
- Hands-on Auftrag mit Filius (Vom Konzept zur Realisierung) 


### Vorgehen:

**1. Auftrag downloaden**

- Downloade den vorbereiteten  [Arbeitsauftrag](m117_Filius_Mittelbetrieb_Subnetting.pdf)

- Öffne das "runtergeladene" File mit einem PDF-Reader

**2. Auftrag durchführen**
 - Arbeitsauftrag durchlesen
 - Textbearbeitungsprogramm (Markdown, Word) öffnen und strukturieren
 - [Vorbereitetes Filius-File](KMU_ca_100MA_Auftrag.fls) downloaden und öffnen
 - Gemäss Kapitel **Vorgehensweise** durchführen

<br>


---

> [⇧ **Zurück zu N2**](../../../README.md)

---


> [⇧ **Zurück zur Hauptseite**](https://gitlab.com/ch-tbz-it/Stud/m117)

---