# M117 - Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren

Dieses Repository enthält Ressourcen für die Lernenden zur Durchführung des Moduls M117 an der TBZ. Es basiert auf den Vorgaben der BIVO2021, bzw. den vorbereitenden Arbeiten von [ModulentwicklungZH M117](https://gitlab.com/modulentwicklungzh/cluster-platform/m117). 


### Hier geht es zu den Inhalten

---
#### [**Zum Hauptverzeichnis**](Unterlagen)

**Unterverzeichnisse**
- [**N1:** Basic Skills](Unterlagen/N1) 
- [**N2:** Advanced Skills](Unterlagen/N2)
- [**N3:** Expert Skills](Unterlagen/N3)
---
- [**LB2** Projektarbeit](Leistungsbeurteilungen/LB2)

#### Ressourcen
- [**Netzwerke Grundlagen**](Ressourcen/NW_2024.pdf) (Herdt Campus: Version 2024)
- [**Netzwerke Netwerktechnik**](Ressourcen/NWTK_2024.pdf) (Herdt Campus: Version 2024)

